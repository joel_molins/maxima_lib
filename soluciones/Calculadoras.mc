/*
Calcula el perímetro de un círculo.
@param radio El radio del círculo
@return El perímetro del círculo
*/
PerimetroCirculo(radio) := 2 * %pi * radio;

/*
Calcula el área de un círculo.
@param radio El radio del círculo
@return El área del círculo
*/
AreaCirculo(radio) := %pi * radio^2;

/*
Calcula el área de la base de un cilindro.
@param radio El radio del círculo de la base
@return El área de la base del cilindro
*/
AreaBaseCilindro(radio) := AreaCirculo(radio);

/*
Calcula el área lateral de un cilindro.
@param radio El radio del círculo de la base
@param altura La altura del cilindro
@return El área lateral del cilindro
*/
AreaLateralCilindro(radio, altura) := PerimetroCirculo(radio) * altura;

/*
Calcula el area total de un cilindro.
@param radio El radio del círculo de la base
@param altura La altura del cilindro
@return El area total del cilindro
*/
AreaTotalCilindro(radio, altura) := 2 * AreaBaseCilindro(radio) + AreaLateralCilindro(radio, altura);

/*
Calcula el volumen de un cilindro.
@param r El radio del círculo de la base
@param h La altura del cilindro
@return El volumen del cilindro
*/
VolumenCilindro(radio, altura) := AreaBaseCilindro(radio) * altura;

/*
Convertir una temperatura en grados Celsius a grados Fahrenheit.
tf = 9.0 / 5 * tc + 32;
@param tc Temperatura Celsius R [-100, 1000]
@return La temperatura equivalente en grados Fahrenheit
*/
ConvertirCelciusToFahrenheit(temperaturaCelcius) := ((9/5) * temperaturaCelcius) + 32;

/*
Convertir una temperatura en grados Fahrenheit a grados Celsius.
@param tf Temperatura Fahrenheit R [-100, 1000]
@return La temperatura equivalente en grados Celsius
*/
ConvertirFahrenheitToCelcius(temperaturaFahrenheit) := (temperaturaFahrenheit - 32) * (5/9);

/*
Convertir un periodo temporal en horas, minutos y segundos en segundos.
@param h El número de horas
@param m El número de minutos
@param s El número de segundos
@return El periodo temporal en segundos
*/
ConversorASegundos(horas, minutos, segundos) := block(
    [ segundosTotales : 0 ], 
    segundosTotales : segundosTotales + horas * 3600,
    segundosTotales : segundosTotales + minutos * 60,
    segundosTotales : segundosTotales + segundos,
    return (segundosTotales)
);

/*
Convertir un periodo temporal en segundos al formato h: m: s.
@param s El número de segundos
@return El periodo temporal en segundos al formato h: m: s
*/
ConversorSegundosAHMS(segundos) := block(
    [ horas, minutos, segundosRestantes ],

    horas : floor(segundos / 3600),
    minutos : floor(segundos / 60) - (horas * 60),
    segundosRestantes : segundos - minutos * 60 - horas * 3600,

    print(sconcat(horas, ":", minutos, ":", segundosRestantes)),
    return ([horas, minutos, segundosRestantes])
);

/*
Calcula el tiempo de vuelo de un avión entre dos aeropuertos.
El tiempo en horas se clacula para t = d/v
@param de La distancia entre dos aeropuertos en km R [100, 40000]
@param v La velocidad del avión en km/h R [200, 1000]
@return El tiempo de vuelos en formato h: m
*/
TiempoVueloAeropuertos(distancia, velocidad) := distancia / velocidad;

/*
Calcula el tiempo de rotación de un satélite en órbita terrestre.
t = 2pi(R+h)**(3/2)/(R*sqrt(g)), g=9.81, R=6371000 es el radio terreste en metros
@param h La altura de la órbita en km R [200, 100000]
@return El tiempo de rotación en formato h: m
*/
CalcularPeriodoSatelite(altura) := block(
    [ g:9.81, R:6371000 ],
    return ( (2 * %pi * (R + altura)) / (R * sqrt(g)) )
);

/*
Calcula el valor absoluto de un número real.
@param a Un número real
@return Su valor absoluto
*/
ValorAbsoluto(numero) := block(
    [ resultado ],
    if numero < 0 then
        (resultado : numero * -1)
    else
        (resultado : numero),
    return (resultado)
);

/*
Calcula el valor máximo de dos números reales.
@param a Un número real
@param b Un número real
@return El valor máximo de ambos números
*/
ValorMaximo(a, b) := block(
    [ maximo : a ],
    if b > maximo then maximo : b,
    return (maximo)
);

/*
Calcula el valor máximo de tres números reales.
@param a Un número real
@param b Un número real
@param c Un número real
@return El valor máximo de los tres números
*/
ValorMaximo(a, b, c) := block(
    [ maximo : a ],
    if b > maximo then maximo : b,
    if c > maximo then maximo : c,
    return (maximo)
);

/*
Convierte un número en base 10 en otra base.
@param a Un número entero positivo
@param b La nueva base Z [2,10]
@return La representación de un número a en la base b
*/
Base10ACualquierBase(numero, nuevaBase) := block(
    [ numeroNuevaBase : 0, parteEntera, residuio, factor : 1 ],

    for i : 1 while numero > 0 do 
        (
            parteEntera : quotient(numero, nuevaBase),
            residuio : remainder(numero, nuevaBase),

            numero : parteEntera,
            numeroNuevaBase : numeroNuevaBase + residuio * factor,

            factor : factor * 10
        ),

    return (numeroNuevaBase)
);

/*
Calcula el máximo común divisor de dos números.
@param a Un número entero positivo
@param b Un número entero positivo
@return El máximo común divisor de ambos números
*/
MCD(a, b) := block (
    [ resultado : 1 ],

    for i : 1 thru a do 
        (
            if remainder(a, i) = 0 and remainder(b, i) = 0 then resultado : i
        ),

    return (resultado)
);

/*
Calcula el mínimo común múltiplo de dos números.
@param a Un número entero positivo
@param b Un número entero positivo
@return El mínimo común múltiplo de ambos números
*/
MCM(a, b) := (a * b) / MCD(a, b);

/*
Calcula la frecuencia de caras obtenidas en el lanzamiento de una moneda.
Nota: No se hace prueba unitaria de integridad de este método. Hacer pruebas "ad-hoc"
@Param n Número de lanzamientos
@Return La frecuencia de las caras en %
*/
FrecuenciaMoneda(lanzamientos) := block(
    [ contador : 0 ],

    for i : 1 thru lanzamientos do 
        (
            if random(2) = 1 then contador : contador + 1
        ),

    return (contador * 100 / lanzamientos)
);
