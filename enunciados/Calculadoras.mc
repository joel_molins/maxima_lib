/*
Calcula el perímetro de un círculo.
@param radio El radio del círculo
@return El perímetro del círculo
*/


/*
Calcula el área de un círculo.
@param radio El radio del círculo
@return El área del círculo
*/


/*
Calcula el área de la base de un cilindro.
@param radio El radio del círculo de la base
@return El área de la base del cilindro
*/


/*
Calcula el área lateral de un cilindro.
@param radio El radio del círculo de la base
@param altura La altura del cilindro
@return El área lateral del cilindro
*/


/*
Calcula el area total de un cilindro.
@param r El radio del círculo de la base
@param h La altura del cilindro
@return El area total del cilindro
*/


/*
Calcula el volumen de un cilindro.
@param r El radio del círculo de la base
@param h La altura del cilindro
@return El volumen del cilindro
*/


/*
Convertir una temperatura en grados Celsius a grados Fahrenheit.
tf = 9.0 / 5 * tc + 32;
@param tc Temperatura Celsius R [-100, 1000]
@return La temperatura equivalente en grados Fahrenheit
*/


/*
Convertir una temperatura en grados Fahrenheit a grados Celsius.
@param tf Temperatura Fahrenheit R [-100, 1000]
@return La temperatura equivalente en grados Celsius
*/

/*
Convertir un periodo temporal en horas, minutos y segundos en segundos.
@param h El número de horas
@param m El número de minutos
@param s El número de segundos
@return El periodo temporal en segundos
*/

/*
Convertir un periodo temporal en segundos al formato h: m: s.
@param s El número de segundos
@return El periodo temporal en segundos al formato h: m: s
*/

/*
Calcula el tiempo de vuelo de un avión entre dos aeropuertos.
El tiempo en horas se clacula para t = d/v
@param de La distancia entre dos aeropuertos en km R [100, 40000]
@param v La velocidad del avión en kmh R [200, 1000]
@return El tiempo de vuelos en formato h: m
*/

/*
Calcula el tiempo de rotación de un satélite en órbita terrestre.
t = 2pi(R+h)**(3/2)/(R*sqrt(g)), g=9.81, R=6371000 es el radio terreste en metros
@param h La altura de la órbita en km R [200, 100000]
@return El tiempo de rotación en formato h: m
*/

/*
Calcula el valor absoluto de un número real.
@param a Un número real
@return Su valor absoluto
*/

/*
Calcula el valor máximo de dos números reales.
@param a Un número real
@param b Un número real
@return El valor máximo de ambos números
*/

/*
Calcula el valor máximo de tres números reales.
@param a Un número real
@param b Un número real
@param c Un número real
@return El valor máximo de los tres números
*/

/*
Convierte un número en base 10 en otra base.
@param a Un número entero positivo
@param b La nueva base Z [2,10]
@return La representación de un número a en la base b
*/

/*
Calcula el máximo común divisor de dos números.
@param a Un número entero positivo
@param b Un número entero positivo
@return El máximo común divisor de ambos números
*/

/*
Calcula el mínimo común múltiplo de dos números.
@param a Un número entero positivo
@param b Un número entero positivo
@return El mínimo común múltiplo de ambos números
*/

/*
Calcula la frecuencia de caras obtenidas en el lanzamiento de una moneda.
Nota: No se hace prueba unitaria de integridad de este método. Hacer pruebas "ad-hoc"
@Param n Número de lanzamientos
@Return La frecuencia de las caras en%
*/